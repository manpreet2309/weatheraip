//
//  InterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Parrot on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON


class InterfaceController: WKInterfaceController, CLLocationManagerDelegate {
    

   
    @IBOutlet var lb1: WKInterfaceLabel!
    
    @IBOutlet var lb2: WKInterfaceLabel!
    
    @IBOutlet var lb3: WKInterfaceLabel!
    @IBOutlet var lb4: WKInterfaceLabel!
    var cityTime = Timer()
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        let APIKey = "0feb7708fad4f1883e453685e0141cacx"
        var lat = 43.76
        var long = -79.42
        let locationManager = CLLocationManager()
        
   
            //requesting for location use
            locationManager.requestWhenInUseAuthorization()
            
            if(CLLocationManager.locationServicesEnabled())
            {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startUpdatingLocation()
            }
       
            
            
            func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
            {
                let location = locations[0]
                lat = location.coordinate.latitude
                long = location.coordinate.longitude
            }
            
            
            
            let URL = "https://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(long)&appid=\(APIKey)&units=metric"
            
            Alamofire.request(URL).responseJSON {
                response in
                let weatherData = response.result.value
                if (weatherData == nil) {
                    print("Error when getting API data")
                    return
                }
                
                let jsonResponse = JSON(weatherData!)
                
                let weatherArray = jsonResponse["weather"].array![0]
                let mainData = jsonResponse["main"]
                
                
                
                self.lb1.setText(jsonResponse["name"].stringValue)
                self.lb2.setText(weatherArray["main"].stringValue)
                self.lb3.setText("\(Int(round(mainData["temp"].doubleValue)))" + "deg C")
                
            }
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        cityTime = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(getTime), userInfo: nil, repeats: true)
        
    }
  
  
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    @objc func getTime(){
        let outputFormat = DateFormatter()
        outputFormat.timeZone = TimeZone(abbreviation:"EST")
        outputFormat.dateFormat = "HH:mm:ss"
        lb4.setText((outputFormat.string(from: Date())))
    }

}
